const demoResponse = {
  cod: '200',
  message: 0,
  cnt: 40,
  list: [
    {
      dt: 1640336400,
      main: {
        temp: 281.37,
        feels_like: 277.85,
        temp_min: 281.37,
        temp_max: 281.54,
        pressure: 1002,
        sea_level: 1002,
        grnd_level: 1001,
        humidity: 93,
        temp_kf: -0.17
      },
      weather: [
        {id: 804, main: 'Clouds', description: 'overcast clouds', icon: '04d'}
      ],
      clouds: {all: 100},
      wind: {speed: 6.84, deg: 259, gust: 12.81},
      visibility: 10000,
      pop: 0.17,
      sys: {pod: 'd'},
      dt_txt: '2021-12-24 09:00:00'
    }
  ],
  city: {
    id: 2743476,
    name: 'Zwolle',
    coord: {lat: 52.5126, lon: 6.0936},
    country: 'NL',
    population: 119030,
    timezone: 3600,
    sunrise: 1640331933,
    sunset: 1640359488
  }
}

export type OpenWeatherMapResponse = typeof demoResponse
export type Forecast = typeof demoResponse.list[0]
