import styles from './styles/Container.module.scss'

interface Props {
  children: React.ReactNode
}

const Container = ({children}: Props) => {
  return (
    <main className={styles.outer}>
      <div className={styles.inner}>{children}</div>
    </main>
  )
}

export default Container
