import format from 'date-fns/format'
import fromUnixTime from 'date-fns/fromUnixTime'
import nl from 'date-fns/locale/nl'
import {Forecast} from '../types/openWeatherMap'
import styles from './styles/ForecastItem.module.scss'

interface Props {
  item: Forecast
}

const ForecastItem = ({item}: Props) => {
  let day = format(fromUnixTime(item.dt), 'EEEE', {locale: nl})
  day = day[0].toUpperCase() + day.slice(1)
  const icon = item.weather[0].icon
  const description = item.weather[0].description
  const temp = Math.round(item.main.temp)
  const precipitation = Math.round(item.pop * 100)
  const humidity = Math.round(item.main.humidity)
  const wind = Math.round(item.wind.speed)

  return (
    <section>
      <h2>{day}</h2>
      <ul className={styles.list}>
        <li>
          <img
            src={`http://openweathermap.org/img/wn/${icon}@2x.png`}
            alt={`Icoon van weersverwachting: ${description}`}
          />
        </li>
        <li className={styles.temperature}>{temp}°C</li>
        <li>Neerslag: {precipitation}%</li>
        <li>Luchtvochtigheid: {humidity}% </li>
        <li>Wind: {wind} m/s</li>
      </ul>
    </section>
  )
}

export default ForecastItem
