import {Forecast} from '../types/openWeatherMap'
import ForecastItem from './ForecastItem'
import styles from './styles/ForecastViewer.module.scss'

interface Props {
  items: Forecast[]
}

const ForecastViewer = ({items}: Props) => {
  return (
    <div className={styles.container}>
      {items.map((forecast) => (
        <ForecastItem item={forecast} key={`${forecast.dt}`} />
      ))}
    </div>
  )
}

export default ForecastViewer
