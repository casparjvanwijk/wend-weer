import styles from './styles/SearchBar.module.scss'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faLocationArrow} from '@fortawesome/free-solid-svg-icons'

interface Props {
  value: string
  loading: boolean
  onChange: (value: string) => void
  onLocationClick: () => void
}

const SearchBar = ({value, loading, onChange, onLocationClick}: Props) => {
  return (
    <div className={styles.container}>
      <input
        className={styles.input}
        value={value}
        placeholder="Stad"
        onChange={(e) => onChange(e.target.value)}
      />
      <div
        className={`${styles.location} ${loading && styles.loading}`}
        onClick={onLocationClick}>
        <FontAwesomeIcon icon={faLocationArrow} />
      </div>
    </div>
  )
}

export default SearchBar
