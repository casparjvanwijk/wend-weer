import './globalStyles/index.scss'
import {useState} from 'react'
import location from './services/location'
import weather from './services/weather'
import Container from './components/Container'
import SearchBar from './components/SearchBar'
import {Forecast} from './types/openWeatherMap'
import ForecastViewer from './components/ForecastViewer'

let debounceTimer: NodeJS.Timeout

const App = () => {
  const [input, setInput] = useState('')
  const [forecasts, setForecasts] = useState<Forecast[] | null>()
  const [loading, setLoading] = useState(false)

  const handleSearchChange = (value: string) => {
    setInput(value)
    clearTimeout(debounceTimer)
    debounceTimer = setTimeout(async () => {
      try {
        const forecasts = await weather.getDailyByCity(value)
        setForecasts(forecasts)
      } catch (err) {
        setForecasts(null)
      }
    }, 250)
  }

  const handleLocationClick = async () => {
    try {
      setLoading(true)
      const coords = await location.getCurrent()
      const {forecasts, city} = await weather.getDailyByCoords(coords)
      setInput(city)
      setForecasts(forecasts)
    } catch (err) {
      console.error(err)
      setForecasts(null)
    } finally {
      setLoading(false)
    }
  }

  return (
    <Container>
      <h1>Wend-Weer</h1>
      <SearchBar
        loading={loading}
        value={input}
        onChange={handleSearchChange}
        onLocationClick={handleLocationClick}
      />
      {forecasts ? <ForecastViewer items={forecasts} /> : 'Geen resultaten.'}
    </Container>
  )
}

export default App
