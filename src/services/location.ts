const getCurrent = () => {
  return new Promise<GeolocationCoordinates>((res, rej) => {
    navigator.geolocation.getCurrentPosition(
      ({coords}) => res(coords),
      (err) => {
        if (err.code === GeolocationPositionError.PERMISSION_DENIED) {
          alert('Je hebt geen toestemming gegeven om je locatie te gebruiken.')
        }
        rej(err)
      }
    )
  })
}

const location = {
  getCurrent
}

export default location
