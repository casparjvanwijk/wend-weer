import axios from 'axios'
import {Forecast, OpenWeatherMapResponse} from '../types/openWeatherMap'

const LANG = 'nl'
const API_KEY = '3e8fc33fd15d277e21bf9e17da0a48f7'

const threeHourToDailyForecasts = (items: Forecast[]) =>
  items.filter((_, index) => index % 8 === 0)

const getDailyByCity = async (city: string) => {
  const res = await axios.get<OpenWeatherMapResponse>(
    `https://api.openweathermap.org/data/2.5/forecast?q=${city}&lang=${LANG}&units=metric&appid=${API_KEY}`
  )
  return threeHourToDailyForecasts(res.data.list)
}

const getDailyByCoords = async ({
  latitude,
  longitude
}: GeolocationCoordinates) => {
  const res = await axios.get<OpenWeatherMapResponse>(
    `https://api.openweathermap.org/data/2.5/forecast?lat=${latitude}&lon=${longitude}&lang=${LANG}&units=metric&appid=${API_KEY}`
  )
  const forecasts = threeHourToDailyForecasts(res.data.list)
  const city = res.data.city.name
  return {forecasts, city}
}

const weather = {
  getDailyByCity,
  getDailyByCoords
}

export default weather
